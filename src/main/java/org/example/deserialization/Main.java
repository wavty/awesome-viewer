package org.example.deserialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String jsonString = "{\"name\":\"Alice\",\"age\":30,\"address\":\"Beijing\"}";

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Person person = objectMapper.readValue(jsonString, Person.class);
            System.out.println(person.getName());
            System.out.println(person.getAge());
            System.out.println(person.getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    // 使用Jackson将一个类对象序列化为String
    public void TestObj2Json() {
        Person.Address address = new Person.Address("beijing", "五道口");
        List<String> hobbies = new ArrayList<>(Arrays.asList("123", "456"));
        Person.Job job = new Person.Job("Engineer", 100);
        Person person = new Person("wavty", 27, address, hobbies, job);

        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonString = mapper.writeValueAsString(person);
            System.out.println(jsonString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    // 使用Jackson将一个String反序列化为类对象
    public void TestJson2Obj() {
        String deserialize = "{\"name\":\"wavty\",\"age\":\"29\",\"address\":{\"city\":\"beijing\",\"street\":\"五道口\"},\"hobbies\":[\"123\",\"456\"],\"job\":{\"title\":\"Engineer\",\"salary\":100}}";
        try {
            ObjectMapper mapper = new ObjectMapper();
            Person person = mapper.readValue(deserialize, Person.class);
            System.out.println(person.getAge());
            System.out.println(person.getHobbies());
            System.out.println(person.getJob());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}