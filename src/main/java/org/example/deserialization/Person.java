package org.example.deserialization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String name;
    private int age;
    private Address address;
    private List<String> hobbies;
    private Job job;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Address {
        private String city;
        private String street;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Job {
        private String title;
        private int salary;
    }
}

