# awesome-viewer

一个 GoLang 玩家觊觎 Java 生态的 Gradle 测试项目。

## 1. 使用 Lombok IDEA 提示找不到符号该如何解决？

需要正确配置build.gradle文件，即需要添加如下两行，其他更为详细的数据可以参考[【Gradle】 Building with Lombok...](https://blog.csdn.net/gh254172840/article/details/122118165)。

```agsl
    compileOnly 'org.projectlombok:lombok:1.18.22'
    annotationProcessor 'org.projectlombok:lombok:1.18.22'
```

## 2. gradle 中几个常用的关键字

当在 Gradle 中定义依赖关系时，可以使用以下几个关键字来指定依赖项的作用域：

| 关键字                   | 说明                                                                 |
|-----------------------|--------------------------------------------------------------------|
| `compileOnly`         | 依赖项只在编译时需要，但在运行时不需要。这通常用于 API 或接口依赖项，而实现由运行时环境提供。                  |
| `annotationProcessor` | 依赖项是一个用于注解处理器的库，它提供了用于生成代码的注解处理器。                                  |  
| `implementation`      | 依赖项在编译和运行时都需要，这是大多数依赖项的默认作用域。                                      |
| `testImplementation`  | 依赖项在编译和测试时都需要，但在运行应用程序时不需要。这通常用于测试框架和工具等。                          |
| `testRuntimeOnly`     | 依赖项在测试运行时需要，但在编译和运行应用程序时不需要。这通常用于运行时测试依赖项，如 JDBC 驱动程序和 Web 测试客户端等。 |

## 3. JSON 风格字符串转为类对象的方式

详见 [org.example.deserialization.Main](./src/main/java/org/example/deserialization/Main.java)。